# Simple Avoidance Algorithm
## INTRODUCTION

Here we want to implement a simple avoidance algorithm to robot. We use e-puck robot, and simulated it on Webots Robot Simulator.

![](misc/robo.gif)

## ROBOT AND SENSORS

### [Robot](https://cyberbotics.com/doc/guide/epuck)

E-puck is a miniature mobile robot originally developed at EPFL for teaching purposes by the designers of the successful Khepera robot. The hardware and software of e-puck is fully open source, providing low level access to every electronic device and offering unlimited extension possibilities.

![](misc/robot.jpeg)

The model includes support for the differential wheel motors (encoders are also simulated, as position sensors), the infra-red sensors for proximity and light measurements, the Accelerometer, the Gyro, the Camera, the 8 surrounding LEDs, the body and front LEDs, bluetooth communication (modeled using Emitter / Receiver devices) and ground sensors extension.


|Feature |	Description|
|--------------|-----------|
|Size 	|7.4 cm in diameter, 4.5 cm high|
|Weight 	|150 g
|Battery 	|about 3 hours with the provided 5Wh LiION rechargeable battery
|Processor 	|Microchip dsPIC 30F6014A @ 60MHz (about 15 MIPS)
|Motors 	|2 stepper motors with 20 steps per revolution and a 50:1 reduction gear
|IR sensors 	|8 infra-red sensors measuring ambient light and proximity of obstacles in a 4 cm range
|Camera 	|color camera with a maximum resolution of 640x480 (typical use: 52x39 or 640x1)
|Microphones 	|3 omni-directional microphones for sound localization
|Accelerometer 	|3D accelerometer along the X, Y and Z axes
|Gyroscope 	|3D gyroscope along the X, Y and Z axes
|LEDs 	|8 red LEDs on the ring and one green LED on the body
|Speaker 	|on-board speaker capable of playing WAV or tone sounds
|Switch 	|16 position rotating switch
|Bluetooth 	|Bluetooth for robot-computer and robot-robot wireless communication
|Remote Control 	|infra-red LED for receiving standard remote control commands

### Sensors

|Device 	|Name|
|--------------|-----------|
|Motors 	|'left wheel motor' and 'right wheel motor'
|Position sensors 	|'left wheel sensor' and 'right wheel sensor'
|Proximity sensors 	|'ps0' to 'ps7'
|Light sensors 	|'ls0' to 'ls7'
|LEDs 	|'led0' to 'led7' (e-puck ring), 'led8' (body) and 'led9' (front)
|Camera 	|'camera'
|Accelerometer 	|'accelerometer'
|Gyro 	|'gyro'
|Ground sensors (extension) 	|'gs0', 'gs1' and 'gs2'
|Speaker 	|'speaker'



The forward direction of the e-puck is given by the positive x-axis of the world coordinates. This is also the direction in which the camera eye and the direction vector of the camera are looking. The axle's direction is given by the positive y-axis. Proximity sensors, light sensors and LEDs are numbered clockwise. Their location and orientation are shown in this figure. The last column of the latter lists the angles between the negative y-axis and the direction of the devices, the plane xOy being oriented counter-clockwise. Note that the proximity sensors and the light sensors are actually the same devices of the real robot used in a different mode, so their direction coincides. Proximity sensor responses are simulated in accordance to the lookup table in this figure; this table is the outcome of calibrations performed on the real robot.


![](misc/sensors_and_leds.png)

## ALGORITHM

### Logic

Now we will discuss the simple avoidance algorithm. Below is the algorithm flow. The algorithm is very simple, just only need two condition and three states.

![](misc/algorithm.jpeg)

The three states are:

1. If the robot detects obstacle on the front right, then the robot is rotate left in place. We use sensor 0 and sensor 1 to detect an obstacle on the front right. If one of the sensors is active, then there is an obstacle on the front right.

![](misc/obs_1.jpeg)

2. If no obstacle on the front right, then the robot detects the obstacle on the front left. If any, then the robot is rotate right in place. We use sensor 7 and sensor 6 to detect the obstacle on the front left. If one of the sensors is active, then there is an obstacle on the front left.

![](misc/obs_2.jpeg)

3. If no obstacle in the front right or front left, then the robot is moving forward.

What about if an obstacle is in front of the robot? If there is an obstacle in front of the robot, then the sensor 0 or sensor 1 is active. So the obstacle front right detection condition is true.

![](misc/obs_3.jpeg)

### Algorithm Extension

There is a flaw in the algorithm. If there are obstacles between robots, then the robot is trapped (see the below figure). Robot detects an obstacle on the front right, so it will rotate to left. After rotate left, the robot detects obstacle on the front left, so the robot will rotate to right. The robot will face this condition continuously, so the robot will trapped between obstacles.

![](misc/obs_4.jpeg)

To overcome this problem, we must add the trapped condition. The robot is trapped between obstacles if sensor 1 and sensor 6 is active. If both of this sensor is active, then the robot is rotated 180⁰ in place as shown below.

![](misc/obs_5.jpeg)

So the final algorithm flow is below. Remember that the condition “Sensor 0 or 1 is Active?” is to detect obstacle on the front right, and condition “Sensor 7 or 6 is Active?” is to detect obstacle on the front left.

![](misc/algorithm_2.jpeg)

## WORLD
![](misc/world.png)